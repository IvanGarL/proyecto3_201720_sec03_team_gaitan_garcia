package api;

import java.io.File;


import model.data_structures.LinearHash;
import model.data_structures.List;
import model.data_structures.Queue;
import model.data_structures.SeparateChainHash;
import model.vo.Componente;
import model.vo.RangoHoraVO;
import model.vo.RetardoVO;
import model.vo.StopVO;
import model.vo.TransferVO;
import model.vo.TripVO;


public interface ISTSManager 
{	
	public void load();
	
	public void printStrongRoutesComponents();
	
	public void printReachableStops(Integer stopId, String fecha);
	
	public void loadDirectStopGraph(Integer stopId, String horaInicial, String horaFinal); 

	public void stops() throws Exception;
	
	public void printStopsGraph();

	public StopVO paradasMasVisitadas();	
	
	public Componente componenteConMasParadas();
	
	public void shortestPathTo(Integer destino);
	
	public void quickestPathTo(Integer destino);
}