package model.data_structures;

import model.vo.VO;

public class LinearHash<V, K>
{
	int n, m;
	K[] keys;
	V[] vals;
	//para una nueva hashtable
	public LinearHash()
	{
		this( 997 );
	}
	//para hacer aumentar la capacidad con rehash()
	public LinearHash(int cap)
	{
		m = cap;
		keys = (K[]) new Object[m];
		vals = (V[]) new Object[m];
		n = 0;
	}
	public void put( K key, V val ) 
	{
		// TODO Auto-generated method stub
		if (n/m >= 0.75) rehash(2*m);
		int i = hash(key);
		for ( ; keys[i] != null; i = (i + 1) % m)
		{
			if (keys[i].equals(key)) 
			{ 
				vals[i] = val; 
				return; 
			}
		}
		keys[i] = key;
		vals[i] = val;
		n++;

	}
	public V get(K key) 
	{
		// TODO Auto-generated method stub
		for(int i = hash(key); keys[i] != null; i = (i + 1) % m)
		{
			if(keys[i].equals(key))
			{
				return vals[i];
			}
		}
		return null;
	}
	public int hash(K key) 
	{
		return ( ( Integer ) key.hashCode() & 0x7fffffff) % m;
	}
	public int size() {
		// TODO Auto-generated method stub
		return n;	
	}
	public void rehash(int cap)
	{
		LinearHash<V, K> t;
		t = new LinearHash<V, K>(cap);
		for (int i = 0; i < m; i++)
		{
			if (keys[i] != null) t.put( keys[i], vals[i] );
		}
		keys = (K[]) t.keys;
		vals = (V[]) t.vals;
		m  = t.m;
	}
	public StructuresIterator<K> iterator() 
	{
		Queue<K> queue = new Queue<K>();
		for (int i = 0; i < m; i++)
			if (keys[i] != null) queue.enqueue(keys[i]);
		return queue.iterator();		
	}
	
	public void delete(K key){
		if(key != null){
			if( !contains(key)) return;
			
			int i = hash(key);
			while(!key.equals(keys[i])){
				i = (i+1) % m;
			}
			
			keys[i] = null;
			vals[i] = null;
			
			i = (i+1) % m;
			while(keys[i] != null){
				K keyToRehash = keys[i];
				V valToRehash = vals[i];
				keys[i] = null;
				vals[i] = null;
				
				n--;
				put(keyToRehash, valToRehash);
				i = (i+1) % m;
			}
			
			n--;
			
			if(n > 0 && n <= m/8) rehash(m/2);
			
			assert check();
		}
	}
	
	public boolean contains(K key){
		return get(key) != null;
	}
	
	   private boolean check() {

	        // check that hash table is at most 50% full
	        if (m < 2*n) {
	            System.err.println("Hash table size m = " + m + "; array size n = " + n);
	            return false;
	        }

	        // check that each key in table can be found by get()
	        for (int i = 0; i < m; i++) {
	            if (keys[i] == null) continue;
	            else if (get(keys[i]) != vals[i]) {
	                System.err.println("get[" + keys[i] + "] = " + get(keys[i]) + "; vals[i] = " + vals[i]);
	                return false;
	            }
	        }
	        return true;
	    }
	
	public StructuresIterator<V> elementsIterator() {
		// TODO Auto-generated method stub
		Queue<V> queue = new Queue<V>();
		for (int i = 0; i < m; i++)
			if (keys[i] != null) queue.enqueue(vals[i]);
		return queue.iterator();			
	}
}