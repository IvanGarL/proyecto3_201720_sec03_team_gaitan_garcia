package model.data_structures;


public class StructuresIterator<T>
{

	protected Node<T> current;
	public StructuresIterator(Node<T> first)
	{
		current = first;
	}
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return current != null;
	}

	public void next() {
		// TODO Auto-generated method stub
		current = current.getNext();
	}
	public void previous()
	{
		current = current.getPrevious();
	}
	public boolean hasPrevious()
	{
		return current.getPrevious() != null;
	}

	public T getElement()
	{
		return current.getElement();
	}
	public boolean contains(T elem)
	{
		if( current == null )
		{
			return false;
		}
		while( current != null )
		{
			if(getElement().equals(elem))
			{
				return true;
			}
			else
			{				
				next();
			}
		}
		return false;
	}
	public Node<T> getCurrent() {
		// TODO Auto-generated method stub
		return current;
	}

}
