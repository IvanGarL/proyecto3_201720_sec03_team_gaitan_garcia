package model.data_structures;

public class HashList < V , K  > 
{
	private HashNode first;
	private HashNode last;
	int n;

	class HashNode
	{  
		K key;
		V val;
		HashNode next;
		public HashNode(K key, V val)
		{
			this.key  = key;
			this.val  = val;
		} 
	}
	public V get(K key)
	{ 
		for(HashNode temp = first; temp != null; temp = temp.next)
		{
			if( key.equals(temp.key)) return temp.val;
		}
		return null;
	}
	public void put(K primary, V val)
	{  
		if( isEmpty() )
		{			
			first = new HashNode(primary, val); 
			last = first;
			n++;
		}
		else 
		{			
			last.next = new HashNode(primary, val);
			last = last.next;
			n++;
		}
	}
	public boolean isEmpty()
	{
		return first == null;
	}
	public StructuresIterator<K> iterator() 
	{
		Queue<K> queue = new Queue<K>();
		for ( HashNode temp = first; temp != null; temp = temp.next)
			queue.enqueue(temp.key);
		return queue.iterator();		
	}
	public StructuresIterator<V> vals() 
	{
		Queue<V> queue = new Queue<V>();
		for ( HashNode temp = first; temp != null; temp = temp.next)
			queue.enqueue(temp.val);
		return queue.iterator();		
	}
	public int size() {
		// TODO Auto-generated method stub
		return n;
	}
	
}



