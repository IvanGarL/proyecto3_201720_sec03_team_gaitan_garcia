package model.data_structures;

import model.exceptions.DateNotFoundException;

public class Stack<T> implements IStack <T> 
{
	Node<T> bottom, top;
	public Stack()
	{
		top = null;
		bottom = top;
	}
	public void push(T item) 
	{
		// TODO Auto-generated method stub
		if(top == null)
		{
			top = new Node<T>(item);
			bottom = top;
		}
		else
		{
			Node<T> temp = new Node<T>(item);
			top.changeNext(temp);
			temp.changePrevious(top);
			top = temp;
		}
	}

	@Override
	public T pop() throws DateNotFoundException 
	{
		// TODO Auto-generated method stub
		if( top == null ) throw new DateNotFoundException("La lista ya se encuentra vacía");
		if(getSize() == 1 ) 
		{
			T deleted = top.getElement();
			top = null;
			bottom = null;
			return deleted;
		}
		else
		{
			T deleted = top.getElement();
			Node<T> temp = top.getPrevious();
			top = temp;
			top.changeNext(null);
			return deleted;
		}
	}

	@Override
	public T get(int pos) throws DateNotFoundException
	{
		// TODO Auto-generated method stub
		int size = getSize();
		if(pos > size - 1 || pos < 0) throw new DateNotFoundException("La posicion se encuentra fuera de los limites de la lista.");
		if(top == null)
		{
			throw new DateNotFoundException("La lista está vacía.");
		}
		else if( pos == 0)
		{
			return bottom.getElement();
		}
		else if(size - 1 == pos )
		{
			return top.getElement();
		}
		else
		{
			Node<T> temp = top.getNext();
			int current = 1;
			boolean stop = false;
			T elem = null;
			while(temp.hasNext() && !stop)
			{
				if(current == pos)
				{
					elem = temp.getElement();
					stop = true;
				}
				temp = temp.getNext();
				current++;
			}
			return elem;
		}
	}

	@Override
	public int getSize(){
		// TODO Auto-generated method stub
		if( top == null ) return 0;
		else
		{
			int cont = 1;
			Node<T> temp = bottom;
			while(temp.hasNext())
			{
				temp = temp.getNext();
				cont++;
			}
			return cont;
		}
	}

	@Override
	public Node<T> getTop() {
		// TODO Auto-generated method stub
		return top;
	}

	@Override
	public Node<T> getBottom() {
		// TODO Auto-generated method stub
		return bottom;
	}
	public StructuresIterator<T> iterator() {
		// TODO Auto-generated method stub
		return new StructuresIterator<T>(bottom);
	}

}
