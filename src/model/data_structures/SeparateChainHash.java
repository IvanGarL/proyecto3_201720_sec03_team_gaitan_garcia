package model.data_structures;

import model.vo.VO;

public class SeparateChainHash<V, K  >
{
	int n, m;
	HashList<V,K>[] hl;
	//para una nueva hashtable
	public SeparateChainHash()
	{
		this( 997 );
	}
	//para hacer aumentar la capacidad con rehash()
	public SeparateChainHash(int cap)
	{
		m = cap;
		hl = (HashList<V, K>[]) new HashList[m];
		for (int i = 0; i < m; i++)
			hl[i] = new HashList<V, K>();
	}
	public void put(K key, V val) 
	{
		// TODO Auto-generated method stub
		HashList<V, K> h = hl[hash(key)];
		if(h.isEmpty()) n++;;
		h.put(key, val);
	}

	public V get(K key) {
		// TODO Auto-generated method stub
		return hl[hash(key)].get(key);
	}

	public int hash(K key) {
		// TODO Auto-generated method stub
		return ( key.hashCode() & 0x7fffffff) % m;
	}

	public int size() {
		// TODO Auto-generated method stub
		n = 0;
		for (int i = 0; i < m; i++) 
		{
			n += hl[i].size();
		}
		return n;
	}

	public void rehash(int cap) {
		// TODO Auto-generated method stub
		SeparateChainHash<V, K> temp = new SeparateChainHash<V, K>(cap);
		for (int i = 0; i < m; i++) {
			StructuresIterator<K> keys = hl[i].iterator();
			for ( ; keys.getCurrent() != null; keys.next()) {
				temp.put( keys.getElement(), hl[i].get(keys.getElement()));
			}
		}
		this.m  = temp.m;
		this.n  = temp.n;
		this.hl = temp.hl;
	}

	public StructuresIterator<K> iterator() {
		// TODO Auto-generated method stub
		Queue<K> queue = new Queue<K>();
		for (int i = 0; i < m; i++) 
		{
			StructuresIterator<K> keys = hl[i].iterator();
			for ( ; keys.getCurrent() != null; keys.next()) 
			{
				queue.enqueue(keys.getElement());
			}
		}
		return queue.iterator();
	}
	public StructuresIterator<V> elementsIterator() {
		// TODO Auto-generated method stub
		Queue<V> queue = new Queue<V>();
		for (int i = 0; i < m; i++) 
		{
			StructuresIterator<V> vals = hl[i].vals();
			for ( ; vals.getCurrent() != null; vals.next()) 
			{
				queue.enqueue(vals.getElement());
			}
		}
		return queue.iterator();		
	}
}
