package model.data_structures;

import model.exceptions.DateNotFoundException;

public interface IStack<T> {

	public void push (T item);
	
	public T pop() throws DateNotFoundException;
	
	public T get(int pos) throws DateNotFoundException;

	public int getSize() ;
	
	public Node<T> getTop();
	
	public Node<T> getBottom();
	
	public StructuresIterator<T> iterator();
}
