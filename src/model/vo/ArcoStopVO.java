package model.vo;

import model.data_structuresII.IArco;

public class ArcoStopVO implements IArco{

	private int peso;
	
	private StopTimeVO origen, destino;
		
	public ArcoStopVO(int peso, StopTimeVO origen, StopTimeVO destino){
		this.peso = peso;
		this.origen = origen;
		this.destino = destino;
	}
	
	public StopTimeVO origen(){
		return origen;
	}
	
	public StopTimeVO destino(){
		return destino;
	}
	@Override
	public int darPeso() {
		// TODO Auto-generated method stub
		return peso;
	}

}
