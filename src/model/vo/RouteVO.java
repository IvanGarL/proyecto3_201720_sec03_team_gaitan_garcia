package model.vo;

import java.awt.Color;

import java.net.URL;

import model.data_structures.LinearHash;
import model.data_structures.Queue;
import model.data_structures.SeparateChainHash;
import model.data_structures.StructuresIterator;

public class RouteVO implements Comparable<RouteVO>, VO<String>
{	
	private int id;
	private String agencyId;
	private String routeNum;
	private String routeName;
	private String routeDesc;
	private String routeUrl;
	private Color routeColor;
	private Color routeTextColor;
	private String agencyName;
	private boolean hasStopId;
	private LinearHash<RetardoVO, Integer> tripsWithDelay;
	private LinearHash<TripVO, Integer> trips;
	private Queue<TripVO> tripsForStop;

	public RouteVO( int pId, String pAngencyId, String pRouteNum, String pRouteName, String pRouteDesc, String pUrl, Color pRouteColor, Color pRouteText)
	{
		id = pId;
		agencyId = pAngencyId;
		routeDesc = pRouteDesc;
		routeColor = pRouteColor;
		routeTextColor = pRouteText;
		routeUrl = pUrl;
		routeNum = pRouteNum;
		routeName = pRouteName;
		tripsWithDelay = new LinearHash<RetardoVO, Integer>();
		trips = new LinearHash<TripVO, Integer>();
		tripsForStop = new Queue<TripVO>();
		hasStopId = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getRouteNum() {
		return routeNum;
	}

	public void setRouteNum(String routeNum) {
		this.routeNum = routeNum;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getRouteUrl() {
		return routeUrl;
	}

	public void setRouteUrl(String routeUrl) {
		this.routeUrl = routeUrl;
	}

	public Color getRouteColor() {
		return routeColor;
	}

	public void setRouteColor(Color routeColor) {
		this.routeColor = routeColor;
	}

	public Color getRouteTextColor() {
		return routeTextColor;
	}

	public void setRouteTextColor(Color routeTextColor) {
		this.routeTextColor = routeTextColor;
	}
	public String toString()
	{
		return id + "";
	}
	public void addTrip(TripVO trip){
		trips.put(trip.darId(), trip);
	}
	public boolean hasStopId(){
		return hasStopId;
	}
	public void loadTripsForStop(Integer stopId){
		StructuresIterator<TripVO> iter = trips.elementsIterator();
		for(; iter.getCurrent() != null; iter.next()){
			iter.getElement().loadStopsForId(stopId);
			if(iter.getElement().hasStopId()){
				hasStopId = true;
				tripsForStop.enqueue(iter.getElement());
			}
		}
	}
	public Queue<TripVO> getTripsForStop(){
		return tripsForStop;
	}
	public LinearHash<TripVO, Integer> getTrips(){
		return trips;
	}
	public LinearHash<RetardoVO, Integer > getTripsWithDelay()
	{
		return tripsWithDelay;
	}
	public String getAgencyName()
	{
		return agencyName;
	}
	public void setAgencyName(String pName)
	{
		agencyName = pName;
	}

	@Override
	public int compareTo(RouteVO r) {
		int comp = getId() - r.getId();
		if(comp > 0) return 1;
		else if(comp < 0) return -1;
		else return 0;
	}

	@Override
	public String darId() {
		// TODO Auto-generated method stub
		return agencyId + "-" + id;
	}

	public void loadTrips(LinearHash<RetardoVO, Integer> retardosTable) {
		// TODO Auto-generated method stub
		StructuresIterator<RetardoVO> i = retardosTable.elementsIterator();
		for(;i.getCurrent() != null;i.next())
		{
			if(i.getElement().getRouteId() == this.id )
			{
				tripsWithDelay.put(i.getElement().darId(), i.getElement());
			}
		}
		
	}
}

