package model.vo;

import model.data_structures.Queue;

public class RangoHoraVO 
{
	int horaInicio;
	int horaFin;
	Queue<StopVO> viajes;
	public RangoHoraVO(int i, int f)
	{
		horaFin = f;
		horaInicio = i;
		viajes = new Queue<StopVO>();
	}
	public int  getInicio()
	{
		return horaInicio;
	}
	public int getFin()
	{
		return horaFin;
	}
	public void addTrip(StopVO s)
	{
		if(!viajes.iterator().contains(s))viajes.enqueue(s);
	}
	public Queue<StopVO> getTrips()
	{
		return viajes;
	}
}
