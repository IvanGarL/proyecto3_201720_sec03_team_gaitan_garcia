package model.vo;

import model.data_structures.LinearHash;
import model.data_structures.Queue;
import model.data_structures.StructuresIterator;

public class AgencyVO implements VO<String>
{
  String id;
  String name;
  String agenUrl;
  String timeZone;
  String language;
  
  Queue<RouteVO> routes;
  
  public AgencyVO(String pId, String pName, String pUrl, String tZone, String lang){
	id = pId;
	name = pName;
	agenUrl = pUrl;
	timeZone = tZone;
	language = lang;
	routes = new Queue<RouteVO>();
  }
  
  public String getId(){
	  return id;
  }
  public String getName(){
	  return name;
  }
  public String getUrl(){
	  return agenUrl;
  }
  public String getTimeZone(){
	  return timeZone;
  }
  public String getLanguage(){
	  return language;
  }
  public void loadRoutes(LinearHash<RouteVO, String> routesTable)
	{
		  StructuresIterator<RouteVO> iter = routesTable.elementsIterator();
		  while(iter.getCurrent() != null)
		  {
			  RouteVO current = iter.getElement();
			  if( current.getAgencyId().equalsIgnoreCase(id) ) current.setAgencyName(this.name); routes.enqueue(current); 
			  iter.next();
		  }
	}
	public Queue<RouteVO> getRoutes()
	{
		return routes;
	}

	@Override
	public String darId() {
		// TODO Auto-generated method stub
		return id;
	}
}
