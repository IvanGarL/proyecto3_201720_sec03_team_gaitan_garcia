package model.vo;

import model.data_structures.LinearHash;
import model.data_structures.SeparateChainHash;
import model.data_structures.StructuresIterator;
import model.data_structuresII.IArco;

public class Componente implements VO<String>, IArco
{
	public Integer routeId;
	public LinearHash<TripVO, Integer> trips;
	public SeparateChainHash<StopTimeVO, Integer> stopTimes;
	public Integer peso;
	public TransferVO transfer;

	public Componente(Integer routeId)
	{
		this.routeId = routeId;
		stopTimes = new SeparateChainHash<StopTimeVO, Integer>();
		trips = new LinearHash<TripVO, Integer>();
		peso = 0;
		transfer = null;
	}

	public String darId() 
	{
		return routeId + "";
	}

	@Override
	public int darPeso()
	{
		return peso;
	}

	public void changeTransfer(TransferVO trans){
		transfer = trans;
	}
	public SeparateChainHash<StopTimeVO,Integer> getStopTimes(){
		return stopTimes;
	}

	public void loadStopTimes()
	{
		for(StructuresIterator<TripVO> t = trips.elementsIterator(); t.getCurrent() != null; t.next()){
			StructuresIterator<StopTimeVO> iter = t.getElement().getStopTimes().elementsIterator();	
			for(; iter.getCurrent() != null; iter.next()){
				if(stopTimes.get(iter.getElement().darId()) == null)
					stopTimes.put(iter.getElement().darId(), iter.getElement());
			}
		}
	}
}