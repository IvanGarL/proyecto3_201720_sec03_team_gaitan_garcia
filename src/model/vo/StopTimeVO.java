package model.vo;

import model.data_structuresII.IArco;

public class StopTimeVO implements VO<Integer>, IArco, Comparable<StopTimeVO>
{
	private Integer tripId;
	private String arrivalTime;
	private String departureTime;
	private Integer stopId;
	private String stopSequence;
	private String stopHeadSign;
	private int pickUpType;
	private int dropType;
	private double distanceTraveled;
	StopVO parada;
	public StopTimeVO(int pId, String pArrivalTime, String pDepartureTime, int pStopId, String pStopSequence, 
			String pStopSign, int pPickupType, int pDropType, double pDistance )
	{
		tripId = pId;
		arrivalTime = pArrivalTime;
		departureTime = pDepartureTime;
		stopId = pStopId;
		stopSequence = pStopSequence;
		stopHeadSign = pStopSign;
		pickUpType = pPickupType;
		dropType = pDropType;
		distanceTraveled = pDistance;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public int getStopId() {
		return stopId;
	}

	public void setStopId(int stopId) {
		this.stopId = stopId;
	}

	public String getStopSequence() {
		return stopSequence;
	}

	public void setStopSequence(String stopSequence) {
		this.stopSequence = stopSequence;
	}

	public String getStopHeadSign() {
		return stopHeadSign;
	}

	public void setStopHeadSign(String stopHeadSign) {
		this.stopHeadSign = stopHeadSign;
	}

	public int getPickUpType() {
		return pickUpType;
	}

	public void setPickUpType(int pickUpType) {
		this.pickUpType = pickUpType;
	}

	public int getDropType() {
		return dropType;
	}

	public void setDropType(int dropType) {
		this.dropType = dropType;
	}

	public double getDistanceTraveled() {
		return distanceTraveled;
	}

	public void setDistanceTraveled(double distanceTraveled) {
		this.distanceTraveled = distanceTraveled;
	}
	public void setStop(StopVO respStop){
		parada = respStop;
	}
	
	public StopVO getStop(){
		return parada;
	}

	public String toString()
	{
		return "" + stopId;
	}
	public int getArrivTime()
	{
		String[] s = arrivalTime.split(":");
		String answ = s[0].trim() + s[1] + s[2];
		return Integer.parseInt(answ);
	}

	public Integer darId() {
		// TODO Auto-generated method stub
		return stopId;
	}
	
	public String getId(){
		return "" + tripId + stopId;
	}

	@Override
	public int darPeso() {
		// TODO Auto-generated method stub
		return tripId+stopId;
	}

	@Override
	public int compareTo(StopTimeVO o) {
		// TODO Auto-generated method stub
		int comp = stopId - o.getStopId();
		if (comp > 0) return 1;
		else if (comp < 0) return -1;
		else return 0;
	}
}
