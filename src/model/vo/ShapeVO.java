package model.vo;

public class ShapeVO {

	String id;
	double latitude;
	double longitude;
	Integer sequence;
	Double disTraveled;
	
	public ShapeVO(String pId, Double pLat, Double pLong, Integer pSeq, Double pDisTravel){
		id = pId;
		latitude = pLat;
		longitude = pLong;
		sequence = pSeq;
		disTraveled = pDisTravel;
	}
	
	public String getId(){
		return id;
	}
	public Double getLatitude(){
		return latitude;
	}
	public Double getLongitude(){
		return longitude;
	}
	public Integer getSequence(){
		return sequence;
	}
	public Double getDistanceTraveled(){
		return disTraveled;
	}
	public String darId(){
		return id;
	}
}
