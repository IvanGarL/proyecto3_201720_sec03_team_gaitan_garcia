package model.vo;


import model.data_structures.LinearHash;
import model.data_structures.Queue;
import model.data_structures.SeparateChainHash;
import model.data_structures.StructuresIterator;

public class CalendarVO  implements VO<String>{

	String serviceId;
	boolean monday;
	boolean tuesday;
	boolean wednesday;
	boolean thursday;
	boolean friday;
	boolean saturday;
	boolean sunday;
	Integer startDt;
	Integer endDt;
	Queue<CalendarDateVO> dates;
	Queue<RetardoVO> retardos;
	Queue<TripVO> trips;
	
	public CalendarVO(String pServiceId, boolean pMonday, boolean pTuesday, boolean pWednesday, boolean pThursday, boolean pFriday, boolean pSaturday, boolean pSunday, Integer pStartDt, Integer pEndDt){
		serviceId = pServiceId;
		monday = pMonday;
		tuesday = pTuesday;
		wednesday = pWednesday;
		thursday = pThursday;
		friday = pFriday;
		saturday = pSaturday;
		sunday = pSunday;
		startDt = pStartDt;
		endDt = pEndDt;
		dates = new Queue<CalendarDateVO>();
		retardos = new Queue<RetardoVO>();
		trips = new Queue<TripVO>();
	}
	
	public String getServiceId(){
		return serviceId;
	}
	public boolean getMonday(){
		return monday;
	}
	public boolean getTuesday(){
		return tuesday;
	}
	public boolean getWednesday(){
		return wednesday;
	}
	public boolean getThursday(){
		return thursday;
	}
	public boolean getFriday(){
		return friday;
	}
	public boolean getSaturday(){
		return saturday;
	}
	public boolean getSunday(){
		return sunday;
	}
	public Integer getStartDt(){
		return startDt;
	}
	public Integer getEndDt(){
		return endDt;
	}
	public void loadDates(LinearHash<CalendarDateVO, String> cdates)
	{
		StructuresIterator<CalendarDateVO> iter = cdates.elementsIterator();
		while( iter.getCurrent() != null )
		{
			CalendarDateVO current = iter.getElement();
			if(current.getServiceId().equals(serviceId)) dates.enqueue(current);
			iter.next();
		}
	}
	public Queue<CalendarDateVO> getDates()
	{
		return dates;
	}
	public void loadRetardos(LinearHash<RetardoVO, Integer> pTrips)
	{
		StructuresIterator<RetardoVO> iter = pTrips.elementsIterator();
		while(iter.getCurrent() != null)
		{
			RetardoVO current = iter.getElement();
			if( current.getServiceId() == Integer.parseInt(serviceId)) retardos.enqueue(current); 
			iter.next();
		}
	}
	public void loadTrips(SeparateChainHash<TripVO, Integer> pTrips)
	{
		StructuresIterator<TripVO> iter = pTrips.elementsIterator();
		while(iter.getCurrent() != null)
		{
			TripVO current = iter.getElement();
			if( current.getServiceId() == Integer.parseInt(serviceId)) trips.enqueue(current); 
			iter.next();
		}
	}
	public Queue<RetardoVO> getRetardos()
	{
		return retardos;
	}
	public Queue<TripVO> getTrips()
	{
		return trips;
	}
	@Override
	public String darId() {
		// TODO Auto-generated method stub
		return serviceId;
	}
}
