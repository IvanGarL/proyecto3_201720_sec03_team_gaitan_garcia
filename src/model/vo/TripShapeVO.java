package model.vo;

import model.data_structures.LinearHash;
import model.data_structures.Queue;
import model.data_structures.StructuresIterator;

/**
 * @author if.garcia
 * CLASE ESPECIFICAMENTE ECHA PARA EL M�TODO 2C
 */
public class TripShapeVO implements Comparable<TripShapeVO>{
	private int routeId;
	private int serviceId;
	private int  tripId;
	private String headSing;
	private String tripShortName;
	private String directionId;
	private int blockId;
	private String shapeId;
	private boolean bikeAllowed;
	private boolean wheelchairFriendly;
	private int arrivalTime;
	private String arrt;
	private Queue<ShapeVO> shapes;
	private double distTraveled;
	private boolean mark;
	
	
	public TripShapeVO(int pRouteID, int pSID, int pTID, String pHS, String pTSN, String pDID, int pBID, String pSHID, int bike, int wheel)
	{
		routeId = pRouteID;
		serviceId = pSID;
		tripId = pTID;
		headSing = pHS;
		tripShortName = pTSN;
		directionId = pDID;
		blockId = pBID;
		shapeId = pSHID;
		if( bike == 0 ) bikeAllowed = false; else bikeAllowed = true;
		if( wheel == 0 ) wheelchairFriendly = false; else wheelchairFriendly = true;
		shapes = new Queue<ShapeVO>();
		arrivalTime = 0;
		distTraveled = 0;
		mark = false;
		
	}
	
	public int getRouteId() {
		return routeId;
	}


	public int getServiceId() {
		return serviceId;
	}

	public int getTripId() {
		return tripId;
	}

	public String getHeadSing() {
		return headSing;
	}

	public String getTripShortName() {
		return tripShortName;
	}

	public String getDirectionId() {
		return directionId;
	}


	public int getBlockId() {
		return blockId;
	}

	public String getShapeId() {
		return shapeId;
	}

	public boolean isBikeAllowed() {
		return bikeAllowed;
	}

	public boolean isWheelchairFriendly() {
		return wheelchairFriendly;
	}
	
	public void Mark(){
		mark = true;
	}
	public boolean isMark(){
		return  mark;
	}

	public void addShape(ShapeVO nueva){
		shapes.enqueue(nueva);
		distTraveled = nueva.getDistanceTraveled();
	}

	public int getArrivalTime(){
		return arrivalTime;
	}
	public double getDistanceTraveled(){
		return distTraveled;
	}
	@Override
	public int compareTo(TripShapeVO aTrip) {
		
		if(getDistanceTraveled() > aTrip.getDistanceTraveled()) return 1;
		else if(getDistanceTraveled() < aTrip.getDistanceTraveled()) return -1;
		else return 0;
	}

	public Integer darId() 
	{
		// TODO Auto-generated method stub
		return tripId;
	}
	public void setDistTraveled(double distTrav){
		distTraveled = distTrav;
	}
	public String getarrt()
	{
		return arrt;
	}

	public Queue<ShapeVO> getShapes(){
		return shapes;
	}
	public LinearHash< TripVO, Integer > retardosEnFechaYRuta(int pRuta, int pFecha)
	{
		 LinearHash< TripVO, Integer > answ = new LinearHash<TripVO, Integer>();
		 return answ;
	}
	/**
	 * Calcula la distancia entre dos puntos
	 * @param lat1 la latidud del punto 1. lat1 != null
	 * @param lon1 la longitud del punto 1. lon1 != null
	 * @param lat2 la latidud del punto 2. lat2 != null
	 * @param lon2 la longitud del punto 2. lon2 != null
	 * @return la distancia entre dos puntos.
	 */
	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{
		final int radius = 1000*6371;
		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.pow(Math.sin(latDistance/2), 2)+ 
				Math.cos(Math.toRadians(lat1))*Math.sin(Math.toRadians(lat2))*Math.pow( Math.sin(lonDistance/2), 2);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return radius*c;
	}
	
}
