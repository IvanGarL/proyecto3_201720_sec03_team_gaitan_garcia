package model.vo;

import model.data_structures.IList;

import model.vo.StopVO;

public class TransferVO implements VO<String>, Comparable<TransferVO>
{
	private String startStopId;
	private String endStopId;
	private Integer transferType;
	
	public TransferVO(String start, String end, Integer ttype, Integer ttime ){
		startStopId = start;
		endStopId = end;
		transferType = ttype;
		transferTime = ttime;
	}
	
	public String getStartId(){
		return startStopId;
	}
	public String getEndId(){
		return endStopId;
	}
	public Integer getTransferType(){
		return transferType;
	}

	/**
	 * Modela el tiempo de transbordo
	 */
	private int transferTime;
	
	/**
	 * Lista de paradas que conforman el transbordo
	 */
	private IList<StopVO> listadeParadas;

	/**
	 * @return the transferTime
	 */
	public int getTransferTime()
	{
		return transferTime;
	}

	/**
	 * @param transferTime the transferTime to set
	 */
	public void setTransferTime(int transferTime) 
	{
		this.transferTime = transferTime;
	}

	/**
	 * @return the listadeParadas
	 */
	public IList<StopVO> getListadeParadas()
	{
		return listadeParadas;
	}

	/**
	 * @param listadeParadas the listadeParadas to set
	 */
	public void setListadeParadas(IList<StopVO> listadeParadas) 
	{
		this.listadeParadas = listadeParadas;
	}

	@Override
	public String darId() {
		// TODO Auto-generated method stub
		return startStopId+"-"+endStopId;
	}

	@Override
	public int compareTo(TransferVO o) {
		// TODO Auto-generated method stub
		if(o.getTransferTime() > transferTime)
			return -1;
		else if(o.getTransferTime() < transferTime)
			return 1;
		else
			return 0;
	}
	

}
