package model.vo;

import java.util.Comparator;

import model.data_structures.LinearHash;
import model.data_structures.List;
import model.data_structures.Queue;
import model.data_structures.StructuresIterator;

public class TripVO implements Comparable<TripVO>, VO <Integer>
{
	private int routeId;
	private int serviceId;
	private int  tripId;
	private String headSing;
	private String tripShortName;
	private String directionId;
	private int blockId;
	private String shapeId;
	private boolean bikeAllowed;
	private boolean wheelchairFriendly;
	private int arrivalTime;
	private LinearHash<StopTimeVO, String> stopTimes;
	private List<StopTimeVO> stopTimesList;
	private Queue<StopTimeVO> stopsForId;
	private Queue<BusUpdateVO> buses;
	private String arrt;
	private Queue<ShapeVO> shapes;
	private double distTraveled;
	private boolean mark;
	private boolean hasStopId;
	

	public TripVO(int pRouteID, int pSID, int pTID, String pHS, String pTSN, String pDID, int pBID, String pSHID, int bike, int wheel)
	{
		routeId = pRouteID;
		serviceId = pSID;
		tripId = pTID;
		headSing = pHS;
		tripShortName = pTSN;
		directionId = pDID;
		blockId = pBID;
		shapeId = pSHID;
		if( bike == 0 ) bikeAllowed = false; else bikeAllowed = true;
		if( wheel == 0 ) wheelchairFriendly = false; else wheelchairFriendly = true;
		stopTimesList = new List<StopTimeVO>();
		stopTimes = new LinearHash<StopTimeVO, String>();
		stopsForId = new Queue<StopTimeVO>();
		buses = new Queue<BusUpdateVO>();
		shapes = new Queue<ShapeVO>();
		arrivalTime = 0;
		distTraveled = 0;
		mark = false;
		hasStopId = false;
		
	}

	public int getRouteId() {
		return routeId;
	}

	public int getServiceId() {
		return serviceId;
	}


	public int getTripId() {
		return tripId;
	}

	public String getHeadSing() {
		return headSing;
	}


	public String getTripShortName() {
		return tripShortName;
	}

	public String getDirectionId() {
		return directionId;
	}

	public int getBlockId() {
		return blockId;
	}

	public String getShapeId() {
		return shapeId;
	}

	public boolean isBikeAllowed() {
		return bikeAllowed;
	}

	public boolean isWheelchairFriendly() {
		return wheelchairFriendly;
	}

	public void Mark(){
		mark = true;
	}
	public boolean isMark(){
		return  mark;
	}
	public String toString()
	{
		return "" + tripId;
	}
	public void addStopTime( StopTimeVO nueva)
	{
		stopTimes.put(nueva.getStopId() + "", nueva);
		stopTimesList.add(nueva);
		arrivalTime = nueva.getArrivTime();
		arrt = nueva.getArrivalTime();
	}
	public void addShape(ShapeVO nueva){
		shapes.enqueue(nueva);
		distTraveled = nueva.getDistanceTraveled();
	}
	public void addBus( BusUpdateVO nueva)
	{
		buses.enqueue(nueva);
	}
	public int getArrivalTime(){
		return arrivalTime;
	}
	public double getDistanceTraveled(){
		return distTraveled;
	}
	
	@Override
	public int compareTo(TripVO aTrip) {
		
		if(getArrivalTime() > aTrip.getArrivalTime()) return 1;
		else if(getArrivalTime() < aTrip.getArrivalTime()) return -1;
		else return 0;
	}

	public Integer darId() 
	{
		return tripId;
	}
	public void setDistTraveled(double distTrav){
		distTraveled = distTrav;
	}
	public String getarrt()
	{
		return arrt;
	}
	public boolean hasStopId(){
		return hasStopId;
	}
	public LinearHash<StopTimeVO, String> getStopTimes()
	{
		return stopTimes;
	}
	public Queue<BusUpdateVO> getBuses()
	{
		return buses;
	}
	public Queue<ShapeVO> getShapes(){
		return shapes;
	}
	
	public Queue<StopTimeVO> getStopsForId(){
		return stopsForId;
	}
	
	public List<StopTimeVO> getStopTimesQueue(){
		return stopTimesList;
	}

	public void loadStopsForId(Integer stopId){
		StructuresIterator<StopTimeVO> iter = stopTimes.elementsIterator();
		for(; iter.getCurrent() != null; iter.next()){
			StopTimeVO nueva = iter.getElement();
			if(nueva.getStopId() == stopId){
				stopsForId.enqueue(nueva);
				hasStopId = true;
			}
		}
	}

	public BusUpdateVO getClosestBus( StopVO stop)
	{
		StructuresIterator<BusUpdateVO> buiter =buses.iterator();
		double less = Double.MAX_VALUE; 
		BusUpdateVO closest = null;
		while( buiter.getCurrent() != null)
		{
			double distance = getDistance(buiter.getElement().getLat(), 
					buiter.getElement().getLon(), stop.getStopLat(), stop.getStopLon());
			if( distance < less)
			{
				less = distance;
				closest = buiter.getElement();
			}
			buiter.next();
		}
		return closest;
	}
	/**
	 * Calcula la distancia entre dos puntos
	 * @param lat1 la latidud del punto 1. lat1 != null
	 * @param lon1 la longitud del punto 1. lon1 != null
	 * @param lat2 la latidud del punto 2. lat2 != null
	 * @param lon2 la longitud del punto 2. lon2 != null
	 * @return la distancia entre dos puntos.
	 */
	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{
		final int radius = 1000*6371;
		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.pow(Math.sin(latDistance/2), 2)+ 
				Math.cos(Math.toRadians(lat1))*Math.sin(Math.toRadians(lat2))*Math.pow( Math.sin(lonDistance/2), 2);
		double c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return radius*c;
	}
	public RetardoVO generarRetardo()
	{
		int pivote = 0;
		int totalRetardos = 0;
		int todasLasParadas = 0;
		RetardoVO nuevo = new RetardoVO(routeId, tripId, serviceId, this);
		for(StructuresIterator<StopTimeVO> stiter = stopTimes.elementsIterator(); stiter.getCurrent() != null ; stiter.next())
		{
			todasLasParadas++;
			StopTimeVO current = stiter.getElement();
			StopVO currentStop = current.getStop();
			BusUpdateVO closest = getClosestBus(currentStop);
			if( closest != null )
			{
				if( current.getArrivTime() - closest.getRecTime() > 60)
				{
					int tiempoRetardo = current.getArrivTime() - closest.getRecTime();
					nuevo.addStop(currentStop);
					nuevo.setDelayTime(tiempoRetardo);
					nuevo.getDelays().get(currentStop.getKey()).sumarRetraso();
					totalRetardos++;
					if(pivote == 0) pivote = todasLasParadas;
				}
			}
		}
		if( pivote + totalRetardos == todasLasParadas ) nuevo.retardar();
		return nuevo;
	}
}
