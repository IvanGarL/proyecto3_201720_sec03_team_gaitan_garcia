package model.vo;

import java.util.Comparator;

public class ComparadorParadaRetrasos implements Comparator<StopVO>{

	@Override
	public int compare(StopVO st1, StopVO st2) {
		if ((st1.getDelays() - st2.getDelays()) > 0) return 1;
		else if ((st1.getDelays() - st2.getDelays()) < 0) return -1;
		else return 0;
	}
	
}