package model.vo;

import java.util.Date;

import model.data_structures.Queue;
import model.data_structures.SeparateChainHash;
import model.data_structures.Queue;
import model.data_structures.StructuresIterator;

public class CalendarDateVO implements VO<String>
{

	String id;
	Integer exceptionDt;
	Integer type;
	Queue<TripVO> trips;
	public CalendarDateVO(String pId, Integer exception,Integer pType){
		id = pId;
		exceptionDt = exception;
		type = pType;
		trips = new Queue<TripVO>();
	}

	public String getServiceId(){
		return id;
	}
	public Integer getExceptionDate(){
		return exceptionDt;
	}
	public Integer getType(){
		return type;
	}
	public void loadTrips(SeparateChainHash<TripVO, Integer> pTrips)
	{
		  StructuresIterator<TripVO> iter = pTrips.elementsIterator();
		  while(iter.getCurrent() != null)
		  {
			  TripVO current = iter.getElement();
			  if( current.getServiceId() == Integer.parseInt(id) ) trips.enqueue(current);
			  iter.next();
		  }
	}
	public Queue<TripVO> getTrips()
	{
		return trips;
	}

	@Override
	public String darId() {
		// TODO Auto-generated method stub
		return id;
	}
}
