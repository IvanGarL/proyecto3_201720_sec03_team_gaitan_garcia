package model.vo;

import model.data_structures.Queue;

import model.data_structures.Queue;
import model.data_structures.StructuresIterator;

public class BusUpdateVO implements VO<Integer>
{

	int tripId;
	String routeNumber;
	String direction;
	String destination;
	String pattern;
	double lat;
	double lon;
	String recordedTime;
	String url;
	String vehicleId;
		
	public BusUpdateVO(String pId, int pTripId, String prouteno, String pdirection, String pdestination, String ppattern, double plat, double plon,
			String ptime, String pURL)
	{
		vehicleId = pId;
		tripId = pTripId;
		routeNumber = prouteno;
		direction = pdirection;
		destination = pdestination;
		pattern = ppattern;
		lat = plat;
		lon = plon;
		recordedTime = ptime;
		url = pURL;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getRouteNumber() {
		return routeNumber;
	}

	public void setRouteNumber(String routeNumber) {
		this.routeNumber = routeNumber;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}
	
	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getRecordedTime() {
		return recordedTime;
	}

	public void setRecordedTime(String recordedTime) {
		this.recordedTime = recordedTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	public int getRecTime()
	{
		String[] s = recordedTime.split(":");
		String answ = s[0] + s[1] + s[2].split(" ")[0];
		if(s[2].split(" ")[1].equals("pm"))
		{			
			return 120000+Integer.parseInt(answ);
		}
		else
		{
			return Integer.parseInt(answ);
		}
	}

	@Override
	public Integer darId() {
		// TODO Auto-generated method stub
		return tripId;
	}

}
