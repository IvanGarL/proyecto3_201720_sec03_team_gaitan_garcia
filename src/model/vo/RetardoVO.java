package model.vo;

import model.data_structures.LinearHash;

public class RetardoVO implements Comparable<RetardoVO>, VO<Integer>
{
	Integer routeId;
	Integer tripId;
	Integer serviceId;
	LinearHash<StopVO, Integer> delayedStops;
	TripVO trip;
	StopVO stop;
	double delayTime;
	boolean mark;
	boolean seRetardoTodo = false;
	public RetardoVO( int pRouteId, int pTripId, int pServiceId, TripVO trip)
	{
		serviceId = pServiceId;
		routeId = pRouteId;
		tripId = pTripId;
		delayedStops = new LinearHash<StopVO, Integer>();
		this.trip = trip;
		delayTime = 0;
		mark = false;
	}
	
	@Override
	public Integer darId() 
	{
		return tripId;
	}
	public Integer getRouteId()
	{
		return routeId;
	}
	public Integer getServiceId()
	{
		return serviceId;
	}
	public void addStop(StopVO nueva)
	{
		delayedStops.put(nueva.getKey(), nueva);
	}
	public void setDelayTime(double time){
		delayTime = time; 
	}
	public double getDelayTime(){
		return delayTime;
	}
	public boolean isMark(){
		return mark;
	}
	public void mark(){
		mark = true;
	}
	@Override
	public int compareTo(RetardoVO o) 
	{
		// TODO Auto-generated method stub
		if(getDelayTime() < o.getDelayTime())
			return 1;
		else if(getDelayTime() > o.getDelayTime())
			return -1;
		else
			return 0;
	}
	public LinearHash<StopVO, Integer> getDelays() {
		return delayedStops;
	}
	public boolean seRetardoTodo()
	{
		return seRetardoTodo;
	}
	public void retardar()
	{
		seRetardoTodo = true;
	}
}
