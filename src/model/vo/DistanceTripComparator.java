package model.vo;

import java.util.Comparator;

public class DistanceTripComparator implements Comparator<TripShapeVO>{
	
	@Override
	public int compare(TripShapeVO o1, TripShapeVO o2) {
		if ((o1.getDistanceTraveled() - o2.getDistanceTraveled() ) > 0) return 1;
		else if ((o1.getDistanceTraveled() - o2.getDistanceTraveled()) < 0) return -1;
		else return 0;		
		}
	
}