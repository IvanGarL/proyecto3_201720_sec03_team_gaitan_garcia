package model.data_structuresII;

import model.vo.VO;

public class Arco<K extends Comparable<K>, V extends VO<K>, A extends IArco> implements Comparable<Arco<K,V,A>>
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	/**
	 * V�rtice desde el cual sale el arco
	 */
	private Vertice<K, V, A> origen;

	/**
	 * V�rtice hacia el cual va el arco
	 */
	private Vertice<K, V, A> destino;

	/**
	 * Elemento en el arco
	 */
	private A infoArco;
	/**
	 * Tipo del arco
	 */
	private int tipo;
	// -----------------------------------------------------------------
	// Constructores
	// -----------------------------------------------------------------

	/**
	 * Constructor del arco
	 * @param pOrigen V�rtice desde el cual sale el arco
	 * @param pDestino V�rtice hacia donde se dirige el arco
	 * @param pInfoArco Elemento en el arco
	 */
	public Arco( Vertice<K, V, A> pOrigen, Vertice<K, V, A> pDestino, A pInfoArco )
	{
		origen = pOrigen;
		destino = pDestino;
		infoArco = pInfoArco;
	}

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Devuelve el elemento del arco
	 * @return Elemento en el arco
	 */
	public A darInfoArco( ){
		return infoArco;
	}

	/**
	 * Devuelve el v�rtice de destino del arco
	 * @return v�rtice de destino del arco
	 */
	public Vertice<K, V, A> darVerticeDestino( ){
		return destino;
	}

	/**
	 * Devuelve el v�rtice de origen del arco
	 * @return v�rtice de origen del arco
	 */
	public Vertice<K, V, A> darVerticeOrigen( ){
		return origen;
	}

	/**
	 * Devuelve el peso del arco
	 * @return Peso del arco
	 */
	public int darPeso( ){
		return infoArco.darPeso( );
	}

	@Override
	public int compareTo(Arco<K, V, A> o) {
		int comp = this.darPeso() - o.darPeso();
		if (comp < 0) return -1;
		else if(comp > 0) return 1;
		else return 0;
	}
	public void setTipo(int tipo)
	{
		this.tipo = tipo;
	}
	public int darTipo()
	{
		return tipo;
	}
}
