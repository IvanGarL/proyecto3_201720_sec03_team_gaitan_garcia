package model.exceptions;

public class ElementNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ElementNotFoundException( String pMsg){
		super(pMsg);
	}
}
