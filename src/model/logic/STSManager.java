package model.logic;

import java.io.BufferedReader;



import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import model.data_structures.LinearHash;
import model.data_structures.List;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainHash;
import model.data_structures.StructuresIterator;
import model.data_structuresII.Camino;
import model.data_structuresII.GrafoDirigido;
import model.data_structuresII.Iterador;
import model.data_structuresII.Lista;
import model.exceptions.ArcoYaExisteException;
import model.exceptions.VerticeNoExisteException;
import model.exceptions.VerticeYaExisteException;
import model.vo.AgencyVO;
import model.vo.ArcoStopVO;
import model.vo.BusUpdateVO;
import model.vo.CalendarDateVO;
import model.vo.CalendarVO;
import model.vo.Componente;
import model.vo.RetardoVO;
import model.vo.RouteVO;
import model.vo.ShapeVO;
import model.vo.StopTimeVO;
import model.vo.StopVO;
import model.vo.TransferVO;
import model.vo.TripShapeVO;
import model.vo.TripVO;
import api.ISTSManager;


public class STSManager implements ISTSManager{

	SeparateChainHash<TripVO, Integer> tripsTable = new SeparateChainHash<TripVO, Integer>(8513);
	SeparateChainHash<StopTimeVO, String> stopTimesTable = new SeparateChainHash<StopTimeVO, String>(9199);
	LinearHash<StopVO, Integer> stopTable = new LinearHash<StopVO, Integer>(3001);
	LinearHash<AgencyVO, String> agenciesTable = new LinearHash<AgencyVO, String>(7);
	LinearHash<RouteVO, String> routesTable = new LinearHash<RouteVO, String>(637);
	LinearHash<RouteVO, Integer> routesTripsTable = new LinearHash<RouteVO, Integer>(637);
	SeparateChainHash<BusUpdateVO, Integer> busUpdateTable = new SeparateChainHash<BusUpdateVO, Integer>(11);
	LinearHash<CalendarVO, String> calendarTable = new LinearHash<CalendarVO, String>(11);
	LinearHash<CalendarDateVO, String> calendarDatesTable = new LinearHash<CalendarDateVO, String>(11);
	SeparateChainHash<TransferVO, String> transfersTable = new SeparateChainHash<TransferVO, String>(3001);
	LinearHash<RetardoVO, Integer> retardosTable = new LinearHash<RetardoVO, Integer>(137);
	SeparateChainHash<TripShapeVO, String> tripsShapeTable = new SeparateChainHash<TripShapeVO, String>(8513);
	SeparateChainHash<ShapeVO, String> shapesTable = new SeparateChainHash<ShapeVO, String>(9199);

	GrafoDirigido<String, Componente, Componente> componentes = new GrafoDirigido<String, Componente, Componente>();
	GrafoDirigido<Integer, StopTimeVO, ArcoStopVO> grafoParadas = new GrafoDirigido<Integer, StopTimeVO, ArcoStopVO>();
	Integer paradaOrigen = 0, horaInicial = 0, horaFinal = 0;

	String stopIds[];
	/**-------------------------------------------
	 * Metodos Auxiliares
	 *--------------------------------------------
	 **/
	private void loadTransfers(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/transfers.txt")));
			reader.readLine();
			String line = reader.readLine();

			while(line != null){

				String data[] = line.split(",");
				String startStopId = data[0];
				String endStopId = data[1];
				Integer transferType = Integer.parseInt(data[2]);
				Integer transferTime;
				if(transferType != 0){
					transferTime = Integer.parseInt(data[3]);
				}
				else transferTime = 0;
				TransferVO nuevo = new TransferVO(startStopId, endStopId, transferType, transferTime);
				if(Integer.parseInt(startStopId) != Integer.parseInt(endStopId)){
					transfersTable.put(nuevo.darId(), nuevo);
				}
				line = reader.readLine();
			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private void loadShapes(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/shapes.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				String id = data[0];
				double shapeLat = Double.parseDouble(data[1]);
				double shapeLong = Double.parseDouble(data[2]);
				Integer sequence = Integer.parseInt(data[3]);
				Double disTraveled = Double.parseDouble(data[4]);
				ShapeVO nueva = new ShapeVO(id, shapeLat, shapeLong, sequence, disTraveled);
				shapesTable.put(nueva.darId(), nueva);
				tripsShapeTable.get(nueva.darId()).addShape(nueva);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	private void readBusUpdate(File rtFile) 
	{
		JSONParser parser = new JSONParser();
		try 
		{			
			Object objects = (Object) parser.parse(new FileReader(rtFile));
			JSONArray list = (JSONArray) objects;
			for(Object temp: list)
			{
				JSONObject current = (JSONObject) temp;
				String id = (String) current.get("VehicleNo");
				int tripid = Integer.parseInt((String) current.get("TripId").toString());
				String routeNo = (String) current.get("RouteNo");
				String direction = (String) current.get("Direction");
				String destination = (String) current.get("Destination");
				String pattern = (String) current.get("Pattern");
				double lat = (Double) current.get("Latitude");
				double lon = (Double) current.get("Longitude");
				String time = (String) current.get("RecordedTime");
				String url = (String) ((JSONObject) current.get("RouteMap")).get("Href");
				BusUpdateVO nuevo = new BusUpdateVO(id, tripid, routeNo, direction, destination, pattern, lat, lon, time, url);
				tripsTable.get(tripid).addBus(nuevo);
				busUpdateTable.put(nuevo.darId() ,nuevo);
			}
		} catch (Exception e) {
		}

	}

	private void loadTrips() 
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/trips.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int routeId = Integer.parseInt(data[0]);
				int serviceId = Integer.parseInt(data[1]);
				int tripId = Integer.parseInt(data[2]);
				String headSing = data[3];
				String tripShortName = data[4];
				String directionId = data[5];
				int blockId = Integer.parseInt(data[6]);
				String shapeId = data[7];
				int wheelchairFriendly = Integer.parseInt(data[8]);
				int bikeAllowed = Integer.parseInt(data[9]);

				TripVO nuevo = new TripVO(routeId, serviceId, tripId, headSing, 
						tripShortName, directionId, blockId, shapeId, bikeAllowed, wheelchairFriendly);
				TripShapeVO nuevoT= new TripShapeVO(routeId, serviceId, tripId, headSing, 
						tripShortName, directionId, blockId, shapeId, bikeAllowed, wheelchairFriendly);

				tripsTable.put(nuevo.darId(), nuevo);
				routesTripsTable.get(nuevo.getRouteId()).addTrip(nuevo);
				tripsShapeTable.put(nuevoT.getShapeId(), nuevoT);
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadRoutes()
	{

		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/routes.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String agencyId = data[1];
				String routeNum = data[2];
				String routeName = data[3];
				String routeDesc = data[4];
				String routeUrl = data[5];
				RouteVO nuevo = new RouteVO(id, agencyId, routeNum, routeName, routeDesc, routeUrl, null, null);
				//nuevo.loadTrips(retardosTable);
				routesTripsTable.put(id, nuevo);
				routesTable.put(nuevo.darId(), nuevo);
				line = reader.readLine();
			}
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	private void loadStopTimes()
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/stop_times.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String arrivalTimeData = data[1];
				String departureTimeData = data[2];
				int stopId = Integer.parseInt(data[3]);
				String stopSequence = data[4];
				String stopSign = data[5];
				int pickUp = Integer.parseInt(data[6]);
				int drop = Integer.parseInt(data[7]);
				double distance;
				if(data.length > 8) 
				{
					distance = Double.parseDouble(data[8]);
				}
				else distance = 0;
				StopTimeVO nuevo = new StopTimeVO(id, arrivalTimeData, departureTimeData, stopId, stopSequence, stopSign, pickUp, drop, distance);
				StopVO h = stopTable.get(stopId);
				h.setTime(nuevo.getArrivTime());
				nuevo.setStop(h);
				h.setStTime(nuevo);
				tripsTable.get(id).addStopTime(nuevo);
				stopTimesTable.put(nuevo.getId(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void loadStops() 
	{
		try
		{
			int i = 0;
			stopIds = new String[8758];
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/stops.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");

				int id = Integer.parseInt(data[0]);
				int code;
				if(data[1] != null && !data[1].equals(" ") && !data[1].isEmpty())
				{					
					code= Integer.parseInt(data[1]);
				}
				else code = 0;
				String name = data[2];
				String stopDesc = data[3];
				double stopLat = Double.parseDouble(data[4]);
				double stopLon = Double.parseDouble(data[5]);
				String stopZone = data[6];
				int locationType = Integer.parseInt(data[8]);
				StopVO nuevo = new StopVO(id, code, name, stopDesc, stopLat, stopLon, stopZone, null, locationType, null);
				stopTable.put( nuevo.darId(), nuevo );
				line = reader.readLine();
				stopIds[i] = id + "";
				i++;
			}
			reader.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private void loadCalendar(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/calendar.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				String serviceId = data[0];
				boolean m = getBoolean(Integer.parseInt(data[1]));
				boolean t = getBoolean(Integer.parseInt(data[2]));
				boolean w = getBoolean(Integer.parseInt(data[3]));
				boolean th = getBoolean(Integer.parseInt(data[4]));
				boolean f = getBoolean(Integer.parseInt(data[5]));
				boolean s = getBoolean(Integer.parseInt(data[6]));
				boolean sn = getBoolean(Integer.parseInt(data[7]));
				Integer startDt = Integer.parseInt(data[8]);
				Integer endDt = Integer.parseInt(data[9]);
				CalendarVO nuevo = new CalendarVO(serviceId,m,t,w,th,f,s,sn,startDt,endDt);
				nuevo.loadDates(calendarDatesTable);
				nuevo.loadRetardos(retardosTable);
				nuevo.loadTrips(tripsTable);
				calendarTable.put(nuevo.darId(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	private void loadCalendarDates(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/calendar_dates.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				String serviceId = data[0];
				Integer exceptionDt = Integer.parseInt(data[1]);
				Integer type = Integer.parseInt(data[2]);
				CalendarDateVO nuevo = new CalendarDateVO(serviceId,exceptionDt,type);
				calendarDatesTable.put(nuevo.darId(), nuevo);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	private void loadAgencies(){
		try{
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/agency.txt")));
			reader.readLine();
			String line = reader.readLine();
			while(line != null){
				String data[] = line.split(",");
				String id = data[0];
				String name = data[1];
				String url = data[2];
				String timeZone = data[3];
				String language = data[4];
				AgencyVO nuevo = new AgencyVO(id, name, url, timeZone, language);
				nuevo.loadRoutes(routesTable);
				agenciesTable.put(nuevo.darId(),nuevo);
				line = reader.readLine();
			}
			reader.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	private boolean getBoolean(int o)
	{
		return o == 1;
	}
	private int diaSemana(String pFecha)
	{	
		int diadesemana = 0;
		try
		{	

			int length = pFecha.length();
			char[]fecha = new char[length];
			pFecha.getChars(0, length, fecha, 0);
			String dia = fecha[6]+""+ fecha[7];
			String mes = fecha[4]+""+fecha[5];
			String ano = fecha[0]+""+fecha[1]+""+fecha[2]+""+fecha[3];
			String a =dia +"/" +mes+ "/" +ano ;
			SimpleDateFormat f1 = new SimpleDateFormat("dd/MM/yyyy");
			Date d1;
			d1 = f1.parse(a);
			DateFormat f2 = new SimpleDateFormat("EEEE");
			String findate = f2.format(d1);
			if (findate.equalsIgnoreCase("lunes"))
			{
				diadesemana = 1;
			}
			else if (findate.equalsIgnoreCase("martes"))
			{
				diadesemana = 2;
			}
			else if (findate.equalsIgnoreCase("miercoles"))
			{
				diadesemana = 3;
			}
			else if (findate.equalsIgnoreCase("jueves"))
			{
				diadesemana = 4;
			}
			else if (findate.equalsIgnoreCase("viernes"))
			{
				diadesemana = 5;
			}
			else if (findate.equalsIgnoreCase("sabado"))
			{
				diadesemana = 6;
			}
			else if (findate.equalsIgnoreCase("domingo")) 
			{
				diadesemana = 0;
			}
		} 
		catch (Exception e) 
		{	

		}
		return diadesemana;
	}	

	private LinearHash<TripVO, Integer> tripsOnDate(String pFecha){	

		int diaSemana = diaSemana(pFecha);
		LinearHash<TripVO, Integer> temp = new LinearHash<TripVO, Integer>();
		StructuresIterator<CalendarVO> c = calendarTable.elementsIterator();

		while(c.getCurrent() != null)
		{
			switch (diaSemana) 
			{
			case 0:
				if(c.getElement().getSunday())
				{
					StructuresIterator<TripVO> t = c.getElement().getTrips().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 1:
				if(c.getElement().getMonday())
				{
					StructuresIterator<TripVO> t = c.getElement().getTrips().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 2:
				if(c.getElement().getTuesday())
				{
					StructuresIterator<TripVO> t = c.getElement().getTrips().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 3:
				if(c.getElement().getWednesday())
				{
					StructuresIterator<TripVO> t = c.getElement().getTrips().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 4:
				if(c.getElement().getWednesday())
				{
					StructuresIterator<TripVO> t = c.getElement().getTrips().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 5:
				if(c.getElement().getFriday())
				{
					StructuresIterator<TripVO> t = c.getElement().getTrips().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 6:
				if(c.getElement().getSaturday())
				{
					StructuresIterator<TripVO> t = c.getElement().getTrips().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			}
			c.next();
		}
		return temp;
	}

	private LinearHash<RetardoVO, Integer> retardosOnDate(String pFecha)
	{	
		int diaSemana = diaSemana(pFecha);
		LinearHash<RetardoVO, Integer> temp = new LinearHash<RetardoVO, Integer>();
		StructuresIterator<CalendarVO> c = calendarTable.elementsIterator();
		while(c.getCurrent() != null)
		{
			switch (diaSemana) 
			{
			case 0:
				if(c.getElement().getSunday())
				{
					StructuresIterator<RetardoVO> t = c.getElement().getRetardos().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 1:
				if(c.getElement().getMonday())
				{
					StructuresIterator<RetardoVO> t = c.getElement().getRetardos().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 2:
				if(c.getElement().getTuesday())
				{
					StructuresIterator<RetardoVO> t = c.getElement().getRetardos().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 3:
				if(c.getElement().getWednesday())
				{
					StructuresIterator<RetardoVO> t = c.getElement().getRetardos().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 4:
				if(c.getElement().getWednesday())
				{
					StructuresIterator<RetardoVO> t = c.getElement().getRetardos().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 5:
				if(c.getElement().getFriday())
				{
					StructuresIterator<RetardoVO> t = c.getElement().getRetardos().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			case 6:
				if(c.getElement().getSaturday())
				{
					StructuresIterator<RetardoVO> t = c.getElement().getRetardos().iterator();
					while( t.getCurrent() != null)
					{
						temp.put(t.getElement().darId(), t.getElement());
						t.next();
					}
				}
				break;
			}
			c.next();
		}
		return temp;
	}

	String getRangoHora( StopVO s)
	{
		for(int i = 0; i < 300000; i += 10000)
		{
			if(s.getTime() <= i + 10000 && s.getTime() > i)
			{
				return i + "-" + (i + 10000);
			}
		}
		return "";
	}


	public void construirGrafo() throws Exception
	{
		for(StructuresIterator<RouteVO> routes = routesTable.elementsIterator(); routes.getCurrent() != null; routes.next()){
			try{
				Componente comp = new Componente(routes.getElement().getId());
				componentes.agregarVertice(comp);
			}catch(Exception e){

			}
		}
		for(StructuresIterator<TripVO> t = tripsTable.elementsIterator(); t.getCurrent() != null; t.next())
		{
			componentes.darVertice(t.getElement().getRouteId() + "").darInfoVertice().trips.put(t.getElement().darId(), t.getElement());
		}
		for(Iterador<Componente> i = componentes.darVertices().darIterador(); i.haySiguiente();)
		{

			Componente c = i.darSiguiente();
			c.loadStopTimes();
		}


		for(Iterador<Componente> i = componentes.darVertices().darIterador(); i.haySiguiente();)
		{
			Componente c1 = i.darSiguiente();
			for(Iterador<Componente> j = componentes.darVertices().darIterador(); j.haySiguiente();)
			{
				Componente c2 = j.darSiguiente();
				for(StructuresIterator<TransferVO> k = transfersTable.elementsIterator(); k.getCurrent() != null; k.next())
				{
					if(c1.getStopTimes().get(Integer.parseInt(k.getElement().getStartId())) != null)
					{
						if(c2.getStopTimes().get(Integer.parseInt(k.getElement().getEndId())) != null)
						{
							try
							{
								TransferVO transfer = k.getElement();
								c1.changeTransfer(transfer);
								c2.changeTransfer(transfer);
								componentes.agregarArco(c1.darId(), c2.darId(), c1);
							} catch(Exception e){
							}
						}
					}
				}
			}
		}
	}
	private void viajesPorParada()
	{
		for (StructuresIterator<TripVO> t = tripsTable.elementsIterator(); t.hasNext(); t.next()) 
		{
			TripVO current = t.getElement();
			for(StructuresIterator<StopTimeVO> s = current.getStopTimes().elementsIterator(); s.hasNext(); s.next())
			{
				s.getElement().getStop().sumarViaje();
			}
		}
	}
	/**-------------------------------------------
	 * Metodos Principales
	 *--------------------------------------------
	 */

	//Metodo auxiliar 1.1
	private LinearHash<TripVO, Integer> tripsWithStop(Integer stopId, String fecha){

		LinearHash<TripVO,Integer> tripsWithStop = new LinearHash<TripVO, Integer>();
		StructuresIterator<TripVO> allTrips = tripsOnDate(fecha).elementsIterator();
		for(; allTrips.getCurrent() != null; allTrips.next()){
			TripVO actual = allTrips.getElement();
			actual.loadStopsForId(stopId);
			if(actual.hasStopId()) tripsWithStop.put(actual.getTripId(), actual);
		}

		return tripsWithStop;
	}

	//Metodo auxiliar 1.2
	private GrafoDirigido<Integer, StopTimeVO, ArcoStopVO> BuildStopsGraph(Integer stopId, String fecha){

		GrafoDirigido<Integer, StopTimeVO, ArcoStopVO> grafoParadas = new GrafoDirigido<Integer, StopTimeVO, ArcoStopVO>();
		StructuresIterator<TripVO> tripsWithStop = tripsWithStop(stopId, fecha).elementsIterator();

		for(; tripsWithStop.getCurrent() != null; tripsWithStop.next()){
			TripVO actual = tripsWithStop.getElement();
			List<StopTimeVO> stopTimes = actual.getStopTimesQueue();
			int i = 0;
			for(i = 1; i < stopTimes.getSize(); i++){
				StopTimeVO paradaA = stopTimes.getElementAtK(i-1);
				StopTimeVO paradaB = stopTimes.getElementAtK(i);

				try {
					grafoParadas.agregarVertice(paradaA);
					grafoParadas.agregarVertice(paradaB);

					int peso = (int) (paradaA.getDistanceTraveled() - paradaB.getDistanceTraveled());
					ArcoStopVO nuevoArco = new ArcoStopVO(peso, paradaA, paradaB);
					grafoParadas.agregarArco(paradaA.getStopId() , paradaB.getStopId() , nuevoArco);

				} catch (VerticeYaExisteException e) {
					boolean A = grafoParadas.existeVertice(paradaA.darId());
					boolean B = grafoParadas.existeVertice(paradaB.darId());

					if(A && !B){
						try {
							grafoParadas.agregarVertice(paradaB);

							int peso = (int) (paradaA.getDistanceTraveled() - paradaB.getDistanceTraveled());
							ArcoStopVO nuevoArco = new ArcoStopVO(peso, paradaA, paradaB);
							grafoParadas.agregarArco(paradaA.getStopId() , paradaB.getStopId() , nuevoArco);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
					else if(B && !A){
						try {
							grafoParadas.agregarVertice(paradaA);

							int peso = (int) (paradaA.getDistanceTraveled() - paradaB.getDistanceTraveled());
							ArcoStopVO nuevoArco = new ArcoStopVO(peso, paradaA, paradaB);
							grafoParadas.agregarArco(paradaA.getStopId() , paradaB.getStopId() , nuevoArco);
						} catch (Exception e2) {
							e2.printStackTrace();
						}
					}
				} catch (VerticeNoExisteException e) {

				} catch (ArcoYaExisteException e) {
					e.printStackTrace();
				}					
			}
		}
		return grafoParadas;
	}

	//Metodo auxiliar 1.3
	private Camino<Integer, StopTimeVO, ArcoStopVO> BFS(StopTimeVO stopVO , GrafoDirigido<Integer, StopTimeVO, ArcoStopVO> grafo, Camino<Integer, StopTimeVO, ArcoStopVO> stopsPath){

		try {
			int i = 0;
			Lista<StopTimeVO> sucesores = grafo.darSucesores(stopVO.darId());
			for(i = 0; i < sucesores.darLongitud() ; i++){
				StopTimeVO actual = sucesores.darElemento(i);
				ArcoStopVO temp =  (ArcoStopVO) (grafo.darArco(stopVO.darId(), actual.darId()));
				stopsPath.agregarArcoFinal(actual, temp);
			}

			Iterador<StopTimeVO> iterB = sucesores.darIterador();
			for(; iterB.haySiguiente();){
				StopTimeVO actual = iterB.darSiguiente();
				BFS(actual, grafo, stopsPath);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stopsPath;
	}

	//Metodo auxiliar 1.4
	private Camino<Integer, StopTimeVO, ArcoStopVO> reachableStops(Integer stopId, String fecha) {

		GrafoDirigido<Integer, StopTimeVO, ArcoStopVO> grafo = BuildStopsGraph(stopId, fecha);
		try {
			Camino<Integer, StopTimeVO, ArcoStopVO> stopsPath = new Camino<Integer, StopTimeVO, ArcoStopVO>(grafo.darVertice(stopId).darInfoVertice());
			stopsPath = BFS(grafo.darVertice(stopId).darInfoVertice(), grafo, stopsPath);
			return stopsPath;
		} catch (VerticeNoExisteException e1) {
			e1.printStackTrace();
			return null;
		}
	}

	/*---------------------
	 * Metodo 1.-----------
	 */
	public void printReachableStops(Integer stopId, String fecha){
		Camino<Integer, StopTimeVO, ArcoStopVO> pathFromStop = reachableStops(stopId, fecha);
		Iterador<ArcoStopVO> arcos = pathFromStop.darArcos().darIterador();
		for(; arcos.haySiguiente();){
			ArcoStopVO actual = arcos.darSiguiente();
			System.out.println("llega a la parada: " + actual.destino().darId() + " - desde la parada: " + actual.origen().darId());
		}
	}

	/*---------------------
	 * Metodo 2.-----------
	 */
	public void printStrongRoutesComponents()
	{
		/**
		 * Recorra todas las componentes recorra los stoptimes del trip principal (llame al atributo) e imprimalos
		 */
		Iterador<Componente> iter = componentes.darVertices().darIterador();
		for(; iter.haySiguiente() ; ){
			Componente act = iter.darSiguiente();
			System.out.println("Ruta: " + act.routeId);
			RedBlackBST<StopTimeVO, Integer> tree = new RedBlackBST<StopTimeVO, Integer>();
			StructuresIterator<TripVO> trips = act.trips.elementsIterator();
			for(; trips.getCurrent() != null; trips.next()){

				StructuresIterator<StopTimeVO> stopTimes = trips.getElement().getStopTimes().elementsIterator();

				for(; stopTimes.getCurrent() != null; stopTimes.next()){
					try{
						if(!tree.contains(stopTimes.getElement().getStopId())){
							tree.put(stopTimes.getElement().getStopId(), stopTimes.getElement());
						}
					}catch(Exception e){
						tree.put(stopTimes.getElement().getStopId(), stopTimes.getElement());
					}
				}
			}
			int i = 0;
			if(!tree.isEmpty()){
				StructuresIterator<StopTimeVO> sortStopTimes = tree.valuesInRange(tree.min(), tree.max());
				for(; sortStopTimes.getCurrent() != null; sortStopTimes.next()){
					if( i == 8) {
						System.out.println();
						i = 0;
					}
					else System.out.print(sortStopTimes.getElement().getStopId() + ", ");
					i++;
				}
				System.out.println();
				System.out.println();
			}
		}
	}

	//M�todo auxiliar 3.2.1
	private RedBlackBST<TripVO, Integer> tripsByStopId(Integer stopId){

		RedBlackBST<TripVO, Integer> tripsBST = new RedBlackBST<TripVO, Integer>();
		List<RouteVO> selectedRoutes = new List<RouteVO>();

		StructuresIterator<RouteVO> routes = routesTripsTable.elementsIterator();
		for(;routes.getCurrent() != null; routes.next()){
			routes.getElement().loadTripsForStop(stopId);
			if(routes.getElement().hasStopId()) selectedRoutes.add(routes.getElement());
		}

		StructuresIterator<RouteVO> routesIter = selectedRoutes.iterator();
		for(; routesIter.getCurrent() != null; routesIter.next()){
			if(componentes.existeVertice(routesIter.getElement().getId() + "")){
				StructuresIterator<TripVO> trips = routesIter.getElement().getTrips().elementsIterator();
				for(; trips.getCurrent() != null; trips.next()) tripsBST.put(trips.getElement().getArrivalTime(), trips.getElement());	
			}
		}

		return tripsBST;
	}

	//M�todo auxiliar 3.2.2
	private StructuresIterator<TripVO> tripsHourRange(Integer stopId, String initialHour, String finalHour){

		Integer hour1 = hour(initialHour);
		Integer hour2 = hour(finalHour);

		RedBlackBST<TripVO, Integer> tripsSelected = tripsByStopId(stopId);

		return tripsSelected.valuesInRange(hour1, hour2);
	}



	/*---------------------
	 * Metodo 3.-----------
	 */
	public void loadDirectStopGraph(Integer stopId, String horaInicial, String horaFinal) 	
	{
		paradaOrigen = stopId;
		this.horaInicial = hour(horaInicial);
		this.horaFinal = hour(horaFinal);
		StructuresIterator<TripVO> tripsWithStop = tripsHourRange(stopId, horaInicial, horaFinal);

		for(; tripsWithStop.getCurrent() != null; tripsWithStop.next()){
			TripVO actual = tripsWithStop.getElement();
			List<StopTimeVO> stopTimes = actual.getStopTimesQueue();
			int i = 0;
			for(i = 1; i < stopTimes.getSize(); i++){
				StopTimeVO paradaA = stopTimes.getElementAtK(i-1);
				StopTimeVO paradaB = stopTimes.getElementAtK(i);

				try {
					grafoParadas.agregarVertice(paradaA);
					grafoParadas.agregarVertice(paradaB);

					int peso = (int) (paradaA.getDistanceTraveled() - paradaB.getDistanceTraveled());
					ArcoStopVO nuevoArco = new ArcoStopVO(peso, paradaA, paradaB);
					grafoParadas.agregarArco(paradaA.getStopId() , paradaB.getStopId() , nuevoArco);

				} catch (VerticeYaExisteException e) {
					boolean A = grafoParadas.existeVertice(paradaA.darId());
					boolean B = grafoParadas.existeVertice(paradaB.darId());

					if(A && !B){
						try {
							grafoParadas.agregarVertice(paradaB);

							int peso = (int) (paradaA.getDistanceTraveled() - paradaB.getDistanceTraveled());
							ArcoStopVO nuevoArco = new ArcoStopVO(peso, paradaA, paradaB);
							grafoParadas.agregarArco(paradaA.getStopId() , paradaB.getStopId() , nuevoArco);
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
					else if(B && !A){
						try {
							grafoParadas.agregarVertice(paradaA);

							int peso = (int) (paradaA.getDistanceTraveled() - paradaB.getDistanceTraveled());
							ArcoStopVO nuevoArco = new ArcoStopVO(peso, paradaA, paradaB);
							grafoParadas.agregarArco(paradaA.getStopId() , paradaB.getStopId() , nuevoArco);
						} catch (Exception e2) {
							e2.printStackTrace();
						}
					}
				} catch (VerticeNoExisteException e) {

				} catch (ArcoYaExisteException e) {
					e.printStackTrace();
				}					
			}
		}

	}


	/*----------------
	 * Metodo 3.1
	 *---------------*/
	public void stops() throws Exception
	{
		Iterador<StopTimeVO> stops = grafoParadas.darVertices().darIterador();
		LinearHash<StopTimeVO, Integer> reps = new LinearHash<StopTimeVO,Integer>();

		for(;stops.haySiguiente();)
		{
			StopTimeVO act = stops.darSiguiente();
			if(reps.get(act.darId()) == null){

				System.out.println("StopId: " + act.darId());
				System.out.println("--- Rutas:");
				Iterador<Componente> compts = componentes.darVertices().darIterador();

				for(; compts.haySiguiente();){
					Componente current = compts.darSiguiente();
					if(current.stopTimes.get(act.darId()) != null){
						System.out.print(current.darId() + ", ");
					}	
				}
				reps.put(act.darId(), act);
				System.out.println();
				System.out.println();
			}
		}
	}

	/*---------------------
	 * Metodo 3.2 & 3.3-----------
	 */
	public void printStopsGraph(){

		Iterador<StopTimeVO> vertices = grafoParadas.darVertices().darIterador();

		for(; vertices.haySiguiente();){
			int i = 0; 
			StopTimeVO actStop = vertices.darSiguiente();
			System.out.println("Parada: " + actStop);
			Lista<Componente> routes = componentes.darVertices();
			for(i = 0; i < routes.darLongitud(); i++){
				Componente actual = routes.darElemento(i);
				if(actual.stopTimes.get(actStop.darId()) != null){
					System.out.println("Se encontro la ruta: " + actual.routeId);
					System.out.println("Sus viajes son:");

					List<TripVO> trips = new List<TripVO>();
					StructuresIterator<TripVO> tripsIter = actual.trips.elementsIterator();

					for(; tripsIter.getCurrent() != null; tripsIter.next()) trips.add(tripsIter.getElement());
					TripVO[] arr = new TripVO[trips.getSize()];
					trips = trips.sort(arr);

					int j = 0;
					for(StructuresIterator<TripVO> trs = trips.iterator(); trs.getCurrent() != null; trs.next()){
						if(j == 10){
							System.out.println();
							j = 0;
						}
						else System.out.print(trs.getElement().getTripId() + ", ");
						j++;
					}
					System.out.println();
					System.out.println();
				}
			}
		}

	}


	/**---------------------
	 * Metodo 3.4
	 *--------------------*/
	public StopVO paradasMasVisitadas()
	{
		int mas = 0;
		String mayor = null;
		for(String id1: stopIds)
		{
			int c = 0;
			for(String id2: stopIds)
			{
				if(id1 != null && id2 != null)
					if(id1.equals(id2))
					{					
						c++;
					}
			}
			if(c > mas)
			{
				mayor = id1;
			}
		}
		return  stopTable.get(Integer.parseInt(mayor));
	}

	/*---------------------
	 * Metodo 3.5 ---------
	 */
	public void shortestPathTo(Integer destino){
		try {
			
			Camino<Integer, StopTimeVO, ArcoStopVO> path = grafoParadas.darCaminoMasCorto(paradaOrigen, destino);
			double tiempo = 0;
			if(path.darLongitud() > 0){
				Iterador<StopTimeVO> stops = path.darSecuenciaVertices();
				for(; stops.haySiguiente();){
					StopTimeVO curr = stops.darSiguiente(); 
					Iterador<Componente> comps = componentes.darVertices().darIterador();
					for(; comps.haySiguiente();){
						
						System.out.println();
						Componente actual = comps.darSiguiente();

						if(actual.stopTimes.get(curr.darId()) != null){

							System.out.println("ID ruta: " + actual.darId());
							System.out.print("ID viajes:");
							int i = 0;
							for(StructuresIterator<TripVO> trips = actual.trips.elementsIterator(); trips.hasNext(); trips.next()){
								if(i == 7){
									System.out.println();
									i = -1;
								}
								else{
									TripVO trip = tripsTable.get(trips.getElement().darId());
									if(trip.getArrivalTime() >= horaInicial && trip.getArrivalTime() <= horaFinal && trip.getStopTimes().get(curr.getStopId()+ "") != null){
										System.out.print(trip.getTripId() + ", ");
										tiempo = ((tiempo+trip.getArrivalTime()) - tiempo);
										i++;
									}
								}
							}
						}
					}
					System.out.println();
					System.out.println();
				}
				
				System.out.println();
				System.out.println("el camino que siguio fue el siguiente");
				Iterador<StopTimeVO> secuencia = path.darSecuenciaVertices();
				int j = 0;
				for (; secuencia.haySiguiente();) {
					if(j == 10){
						System.out.println();
						j = -1;
					}
					System.out.print(secuencia.darSiguiente() + " -> ");
					j++;
				}
				System.out.println();
				System.out.println("tiempo total = " + tiempo + "(s)");
			} else{
				System.out.println("No se encontro camino a la parada destino");
			}
		} catch (VerticeNoExisteException e) {
			e.printStackTrace();
		}
	}
	
	/*---------------------
	 * Metodo 3.6 ---------
	 */
	public void quickestPathTo(Integer destino){
		try {
			
			Camino<Integer, StopTimeVO, ArcoStopVO> path = grafoParadas.darCaminoMasBarato(paradaOrigen, destino);
			double tiempo = 0;
			if(path.darLongitud() > 0){
				Iterador<StopTimeVO> stops = path.darSecuenciaVertices();
				for(; stops.haySiguiente();){
					StopTimeVO curr = stops.darSiguiente(); 
					Iterador<Componente> comps = componentes.darVertices().darIterador();
					
					for(; comps.haySiguiente();){
						Componente actual = comps.darSiguiente();

						if(actual.stopTimes.get(curr.darId()) != null){

							System.out.println("ID ruta: " + actual.darId());
							System.out.print("ID viajes:");
							int i = 0;
							for(StructuresIterator<TripVO> trips = actual.trips.elementsIterator(); trips.hasNext(); trips.next()){
								if(i == 7){
									System.out.println();
									i = -1;
								}
								else{
									TripVO trip = tripsTable.get(trips.getElement().darId());
									if(trip.getArrivalTime() > horaInicial && trip.getArrivalTime() < horaFinal && trip.getStopTimes().get(curr.getStopId()+ "") != null){
										System.out.print(trip.getTripId() + ", ");
										tiempo = ((tiempo+trip.getArrivalTime()) - tiempo);
										i++;
									}
								}
							}
						}
					}
					System.out.println();
				}
				
				System.out.println();
				System.out.println("el camino que siguio fue el siguiente");
				Iterador<StopTimeVO> secuencia = path.darSecuenciaVertices();
				int j = 0;
				for (; secuencia.haySiguiente();) {
					if(j == 10){
						System.out.println();
						j = -1;
					}
					System.out.print(secuencia.darSiguiente() + " -> ");
					j++;
				}
				System.out.println();
				System.out.println("tiempo total = " + tiempo + "(s)");
			} else{
				System.out.println("No se encontro camino a la parada destino");
			}
		} catch (VerticeNoExisteException e) {
			e.printStackTrace();
		}
	}

	/**---------------------
	 * Metodo 3.7
	 *--------------------*/
	public Componente componenteConMasParadas()
	{
		Componente mas = null;
		int mayor = 0;
		for(Iterador<Componente> i = componentes.darVertices().darIterador(); i.haySiguiente();)
		{
			Componente act = i.darSiguiente();
			if(act.stopTimes.size() > mayor)
			{
				mayor = act.stopTimes.size();
				mas = act;
			}
		}

		int c = 0;
		String print = "";
		System.out.println("La componente fuertemente conectada m�s grande es: " + mas.darId() + " con: " + mayor + " paradas. Sus ids:");
		for(StructuresIterator<StopTimeVO> st = mas.stopTimes.elementsIterator(); st.hasNext(); st.next())
		{
			if(c < 12)
			{
				print += st.getElement().getStopId() + " - ";
				c++;
			}
			else
			{				
				System.out.println(print);
				c = 0;
				print = "";
			}
		}
		return mas;
	}

	/**--------------------
	 * Cargar
	 *-------------------*/
	public void load() 
	{

		loadRoutes();
		System.out.println("Cargo rutas");
		loadTrips();
		System.out.println("Cargo viajes");
		loadStops();
		System.out.println("Cargo paradas");
		loadStopTimes();
		System.out.println("Cargo tiempos para paradas");
		loadCalendar();
		System.out.println("Cargo calendario");
		loadCalendarDates();
		System.out.println("Cargo fechas");
		loadAgencies();
		System.out.println("Cargo agencias");
		loadTransfers();
		System.out.println("Cargo transbordos");
		loadShapes();
		System.out.println("Cargo shapes");
		try {
			construirGrafo();
			System.out.println("Cargo el grafo");
		} catch (Exception e) {
			e.printStackTrace();
		}
		viajesPorParada();
	}
	private int hour(String txt)
	{
		String[] HMS = txt.split(":");
		String hour = HMS[0] + HMS[1] + "00";
		return Integer.parseInt(hour);
	}
}