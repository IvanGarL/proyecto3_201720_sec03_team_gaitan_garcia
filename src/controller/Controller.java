package controller;

import java.io.File;


import model.data_structures.LinearHash;
import model.data_structures.List;
import model.data_structures.Queue;
import model.data_structures.SeparateChainHash;
import model.logic.STSManager;
import model.vo.Componente;
import model.vo.RangoHoraVO;
import model.vo.RetardoVO;
import model.vo.StopVO;
import model.vo.TransferVO;
import model.vo.TripVO;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void load() 
	{
		manager.load();
	}
	
	public static void routesStrongComponents(){
		manager.printStrongRoutesComponents();
	}
	
	public static void reachableStops(Integer stopId, String fecha){
		manager.printReachableStops(stopId, fecha);
	}
	
	public static void stops() throws Exception
	{
		manager.stops();
	}
	
	public static StopVO paradasMasVisitadas()
	{
		return manager.paradasMasVisitadas();
	}
	
	public static void loadDirectStopGraph(Integer stopId, String horaInicial, String horaFinal){
		manager.loadDirectStopGraph(stopId, horaInicial, horaFinal);
	}
	
	public static void printStopsGraphItinerary(){
		manager.printStopsGraph();
	}
	
	public static Componente componenteConMasParadas()
	{
		return manager.componenteConMasParadas();
	}
	
	public static void shortestPathTo(Integer destino){
		 manager.shortestPathTo(destino);
	}
	
	public static void quickestPathTo(Integer destino){
		manager.quickestPathTo(destino);
	}
}
