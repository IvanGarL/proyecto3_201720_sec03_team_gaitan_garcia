package view;

import java.util.Scanner;

import model.data_structures.LinearHash;
import model.data_structures.List;
import model.data_structures.SeparateChainHash;
import model.data_structures.StructuresIterator;
import model.vo.StopVO;
import model.vo.TransferVO;
import model.vo.TripVO;
import controller.Controller;

public class STSManagerView {

	public static final String FECHA = "20170821";
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();
			int a = 0;
			switch(option){
			case 0:
				double startTime = System.currentTimeMillis();
				Controller.routesStrongComponents();
				double endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
			case 1:
				if(a > 0 ){
					System.out.println("Ya se cargaron los datos una vez."); break;}
				startTime = System.currentTimeMillis();
				System.out.println("Cargando la información, esto se puede tardar unos segundos...");
				Controller.load();
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				a++;
				break;
			case 2:
				System.out.println("Ingrese el id de la parada");
				Integer stopId= sc.nextInt();
				startTime = System.currentTimeMillis();
				Controller.reachableStops(stopId, FECHA);
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 3:
				startTime = System.currentTimeMillis();
				Controller.routesStrongComponents();
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 4:
				System.out.println("Ingrese el id de la parada");
				stopId = sc.nextInt();
				System.out.println("Ingrese la hora de inicio en hh:mm(e.g. 200000)" );
				String initialHour = sc.next();
				System.out.println("Ingrese la hora de fin en hh:mm (e.g. 200000)" );
				String finalHour = sc.next();
				startTime = System.currentTimeMillis();
				Controller.loadDirectStopGraph(stopId, initialHour, finalHour);
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 5:
				startTime = System.currentTimeMillis();
				try {
					Controller.stops();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 6:
				startTime = System.currentTimeMillis();
				Controller.printStopsGraphItinerary();
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 7:
				startTime = System.currentTimeMillis();
				Controller.printStopsGraphItinerary();
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 8:
				startTime = System.currentTimeMillis();
				StopVO s = Controller.paradasMasVisitadas();
				System.out.println("Id: " + s + " Rutas: Todas" + " Viajes: " + s.darNumViajes());
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 9:
				System.out.println("Ingrese el id de la parada de destino");
				stopId = sc.nextInt();
				startTime = System.currentTimeMillis();
				Controller.shortestPathTo(stopId);
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 10:
				System.out.println("Ingrese el id de la parada de destino");
				stopId = sc.nextInt();
				startTime = System.currentTimeMillis();
				Controller.quickestPathTo(stopId);
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 11:
				startTime = System.currentTimeMillis();
				Controller.componenteConMasParadas();
				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 12:	
				startTime = System.currentTimeMillis();

				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 13:	
				startTime = System.currentTimeMillis();

				endTime = System.currentTimeMillis();
				System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");
				break;
			case 14:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("--------------------Proyecto 3---------------------");
		System.out.println("1. (1) Cargar Datos");
		System.out.println("1. (2) Paradas alcanzables desde una parada X");
		System.out.println("2. (3) Componentes fuertemente conectadas de Rutas");
		System.out.println("3. (4) Hacer grafo de paradas para un rango de horas");
		System.out.println("3.1 (5) Rutas que pueden llegar a una parada, para todas las paradas");
		System.out.println("3.2 (6) Intinerario de paradas por orden de llegada");
		System.out.println("3.3 (7) Intinerario de paradas por orden de salida");
		System.out.println("3.4 (8) Parada mas visitada.");
		System.out.println("3.5 (9) Camino mas corto a parada X");
		System.out.println("3.6 (10) Caminos menos demorado a parada X");
		System.out.println("3.7 (11) La componente con mas paradas.");
		System.out.println("3.8 (12) ");
		System.out.println("3.9 (13) ");
		System.out.println("---------------------------------------------------");
		System.out.println("14. Salir");
		System.out.println("Ingrese el número de la tarea: (e.g., 1):");
	}
}
